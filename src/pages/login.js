import React, { useState, useEffect, useContext } from 'react';
import {Form ,Button} from 'react-bootstrap';
import Swal from "sweetalert2";
import UserContext from '../UserContext';
import { Navigate, useNavigate } from 'react-router-dom';


export default function Login(){
    const navigate = useNavigate();
    //Allows us to consume the User context object and it's properties to use for user validation
    const { user, setUser } = useContext(UserContext);


    // State hooks to store the values of the input fields
    const [email, setEmail]  = useState('');
    const [password, setPassword] = useState('');
    //login button
    const [isActive, setIsActive] = useState(false);

    useEffect(()=>{
        //Validation to enable submit button
        if (email !== '' && password !== ''){
            setIsActive(true);
        }else{
            setIsActive(false);
        }
    }, [email,password])

	return(
		(user.accessToken !== null) ?

        <Navigate to="/courses" />

        :

        <Form>
            <h1>Login</h1>
            <Form.Group>
                <Form.Label>Email address:</Form.Label>
                <Form.Control 
                    type="email"
                    placeholder="Enter email"
                    required
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}

                />
            </Form.Group>

            <Form.Group>
                <Form.Label>Password:</Form.Label>
                <Form.Control 
                    type="password"
                    placeholder="Enter your password"
                    required
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    
                />
            </Form.Group>

            {isActive ?
                <Button variant="primary" type="submit" className="mt-3"> Login </Button>

                :

                <Button variant="danger" type="submit" disabled className="mt-3"> Login </Button>
            }

            
        </Form>
		)
}